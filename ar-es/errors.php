<?
$file_rev="041305";
$file_lang="en";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// Error title and headers..
$LANG_error="ERROR";
$LANG_error_header="Se encontraron los siguiente errores al procesar tu requerimiento:";
$LANG_back="Atras";
$LANG_tryagain="Por favor <a href=\"javascript:history.go(-1)\">vuelve</a> e int&eacute;ntalo de nuevo";

// Login error stuff
$LANG_login_error="Los datos de login ingresados son incorrectos! 
	Por favor <a href=\"index.php\">vuelve</a> e int&eacute;ntalo de nuevo!";
// Client link is different..so we have to repeat this.
$LANG_login_error_client=$LANG_login_error="Los datos ingresados son incorrectos! 
	Por favor <a href=\"../index.php\">vuelve</a> e int&eacute;ntalo de nuevo!";

// mysql connect error (used in both login and processing)
$LANG_error_mysqlconnect="No fue posible conectar con la base de datos con la informaci&oacute;n provista. 
	La base de datos retorn&oacute; el siguiente mensaje:";

// ADMIN: add account error: We only check the username
// because we expect the admin to know what he/she is doing...
$LANG_addacct_error="El nombre de usuario ya existe! 
	Por favor <a href=\"javascript:history.go(-1)\">vuelve</a> e int&eacute;ntalo de nuevo";


// ADMIN: add admin errors..
$LANG_adminconf_login_long="La informaci&oacute;n enviada en el campo Login es inv&aacute;lida. (debe tener menos de 20 caracteres)";
$LANG_adminconf_login_short="La informaci&oacute;n enviada en el campo Login es inv&aacute;lida. (debe tener al menos 2 caracteres)";
$LANG_adminconf_login_inuse="ID $newlogin ya est&aacute; en uso";
$LANG_adminconf_pw_mismatch="Las contrase&ntilde;as no coinciden";
$LANG_adminconf_pw_short="La informaci&oacute;n enviada en el campo Contrase&ntilde;a es inv&aacute;lida. (debe tener al menos 4 caracteres)";
$LANG_adminconf_goback="Por favor <a href=\"javascript:history.go(-1)\">vuelve</a> e int&eacute;ntalo de nuevo";
$LANG_adminconf_added="La cuenta ha sido agregada";

// ADMIN: Category admin errors...
$LANG_addcat_tooshort="El nombre de la categor&iacute;a debe tener al menos 2 caract&eacute;res";
$LANG_addcat_toolong="El nombre de la categor&iacute;a no debe tener mas de 50 caract&eacute;res";
$LANG_addcat_exists="El nombre de la categor&iacute;a ya existe";
$LANG_cats_nocats="No hay categor&iacute;as disponibles! Debe haber al menos una categor&iacute;a!";
$LANG_delcat_default="No es posible eliminar la categor&iacute;a por defecto!";

// ADMIN/CLIENT: Change pw errors...
$LANG_pwconfirm_err_mismatch="Las contrase&ntilde;as no coinciden!";
$LANG_pwconfirm_err_short="La informaci&oacute;n enviada en el campo Contrase&ntilde;a es inv&aacute;lida. (debe tener al menos 4 caracteres).";
$LANG_pwconfirm_err_intro="Se encontraron los siguiente errores al procesar tu requerimiento";

// ADMIN/CLIENT: Upload/banner Errors...
$LANG_upload_blank="No has ingresado el acceso a una imagen v&aacute;lida. Por favor <a href=\"javascript:history.go(-1)\">vuelve</a> e int&eacute;ntalo de nuevo";
$LANG_upload_not="La imagen no se carg&oacute;. Por favor <a href=\"javascript:history.go(-1)\">vuelve</a> e int&eacute;ntalo de nuevo";
$LANG_err_badimage="El sistema no pudo localizar la imagen de tu banner con la URL provista. 
	Esto podr&iacute;a ser porque no es una imagen, o la imagen no existe en la URL provista (<b>$bannerurl</b>) 
	si has subido esta imagen, pudo haber algun problema durante la carga del archivo.  
	Por favor verifica la URL e int&eacute;ntalo de nuevo.  
	Por favor nota que si tu iamgen esta localizada en un servidor gratuito tal como Geocities o Angelfire, 
	tal vez debas incluir tu banner en alguna parte de tu pagina a fin de que pueda ser vinculado de modo remoto. 
	Algunos servidores no permiten vinculado remoto en absoluto.";
$LANG_err_badwidth="Tu banner es inv&aacute;lido porque es demasiado ancho. Los banners de este intercambio deben tener $bannerwidth pixels de ancho.";
$LANG_err_badheight="Tu banner es inv&aacute;lido porque es demasiado alto. Los banners de este intercambio deben tener $bannerheight pixels de alto.";
$LANG_err_filesize="El tama�o de archivo de tu banner excede el m&aacute;ximo permitido en el intercambio. El banner no debe pesar mas de $max_filesize bytes.";

// Add admin error..
$LANG_adminconf_loginexist="El ID de login especificado ya est&aacute; en uso!";

// edit templates error..
$LANG_editcsstemplate_errornofile="El archivo especificado no existe! Esto sucede usalmente si has cambiado el nombre del archivo o si has intentado pasar una variable con un nombre de archivo incorrecto. Por motivos de seguridad, esto no esta permitido. Por favor vuelve e int&eacute;ntalo de nuevo.";
$LANG_editcsstemplate_cannotwrite="phpBannerExchange no puede escribir en el archivo: css.php. Esto puede deberse a que los permisos del archivo sean incorrectos (debe ser 755 or 777) o que no tengas acceso para escritura en este archivo. Verifica los permisos del archivo y sus derechos de acceso e int&eacute;ntalo de nuevo.";

// Promo Manager errors..
$LANG_promo_noproduct="No has ingresado el nombre del producto!";
$LANG_promo_badcode="No has ingresado un c&oacute;digo o el c&oacute;digo ya esta en uso!";
$LANG_promo_noval="Debes ingresar un valor en el campo \"value\" para poder usar este tipo de promo.";
$LANG_promo_nocreds="Debes ingresar un valor en el campo \"credits\" para poder usar este tipo de promo.";

// "COMMON"/PUBLIC SECTION ERRORS

// Lost Password error -- unable to locate account.
$LANG_lostpw_noacct="No se pudo encontrar una cuenta de <b>{email}</b>. Por favor vuelve a intentarlo.";

// Signup Errors (/signupconfirm.php)
$LANG_err_nametooshort="La informaci&oacute;n enviada en el campo <b>Real Name</b> es inv&aacute;lida. (debe tener mas de 2 caracteres.- Ingresaste: <b>$_REQUEST[name]</b>)";
$LANG_err_nametoolong="La informaci&oacute;n enviada en el campo <b>Real Name</b> es inv&aacute;lida. (debe tener menos de 100 caracteres.- Ingresaste: <b>$_REQUEST[name]</b>)";
$LANG_err_loginshort="La informaci&oacute;n enviada en el campo <b>Username</b> es inv&aacute;lida. (debe tener menos de 20 caracteres.- Ingresaste: <b>$_REQUEST[login]</b>)";
$LANG_err_loginlong="La informaci&oacute;n enviada en el campo <b>Username</b> es inv&aacute;lida. (debe tener al menos 2 caracteres de largo.- Ingresaste: <b>$_REQUEST[login]</b>)";
$LANG_err_logininuse="El nombre de usuario $_REQUEST[login] ya est&aacute; en uso";
$LANG_err_emailinuse="La <b>direcci&oacute;n de e-mail</b> especificada, <b>$_REQUEST[email]</b>, ya est&aacute; en uso. 
	Solo se acepta una cuenta por direcci&oacute;n de e-mail";
$LANG_err_invalidurl="La URL del sitio es inv&aacute;lida. Por favor asegurate de incluir el nombre de archivo (index.html, por ejemplo) 
	o la barra al final (http://www.algunsitio.com/)! Ingresaste: <b>$_REQUEST[targeturl]</b>";
$LANG_err_badimage="El sistema no pudo localizar la imagen de tu banner con la URL que enviaste. 
	Esto podria deberse a que quizas no sea una imagen, o a que has escrito mal la URL de la misma (<b>$_REQUEST[bannerurl]</b>). 
	Por favor verfica la URL e int&eacute;ntalo de nuevo.  Debes notar que si tu banner esta en un servidor gratuito tal como Geocities o 
	Angelfire, quiz&aacute;s debas incluir el banner en alguna parte de tu propia p&aacute;gina a fin de que pueda ser vinculado de manera remota. 
	Algunos servicios gratuitos no permiten vinculaci&oacute;n remota en absoluto.";
$LANG_err_badwidth="Tu banner es inv&aacute;lido porque tiene <b>$imagewidth</b> pixels de ancho. Los banners de este intercambio deben tener <b>$bannerwidth</b> pixels de ancho.";
$LANG_err_badheight="Tu banner es inv&aacute;lido porque tiene <b>$imageheight</b> pixels se alto. Los banners de este intercambio deben tener <b>$bannerheight</b> pixels de alto.";
$LANG_err_email="El sistema fue incapaz de validar tu direcci&oacute;n de e-mail porque contiene caracteres especiales. 
	Por favor contacta al administrador para m&aacute;s asistencia.- (Ingresaste: <b>$_REQUEST[email]</b>).";
$LANG_err_passmismatch="Las contrase&ntilde;as no coinciden! <b>$_REQUEST[pass]</b> no es igual a <b>$_REQUEST[pass2]</b>";
$LANG_err_passshort="La informaci&oacute;n enviada en el campo <b>Contrase&ntilde;a</b> es inv&aacute;lida. (debe tener al menos 4 caracteres.- Ingresaste <b>$_REQUEST[pass]</b>).";
$LANG_err_nocoupon="El cup&oacute;n que has ingresado no es v&aacute;lido o es de un tipo inv&aacute;lido! Los Cupones son CaSe SeNsItIvE!";

// Client coupon errors...
$LANG_coupon_wrongtype="Este tipo de cup&oacute;n no puede aplicarse en el online store!";
$LANG_coupon_nocoup="El cup&oacute;n que has ingresado no es v&aacute;lido! Los Cupones son CaSe SeNsItIvE!";

// Promo code errors..
$LANG_coupon_clntwrongtype="Este tipo de cup&oacute;n solo puede ser aplicado en el online store!";
$LANG_coupon_noreuse="Este cup&oacute;n no puede ser reutilizado!";
$LANG_coupon_userwrongtype="No puedes utilizar este cup&oacute;n, solo puede ser utilizado al registrarte!";
$LANG_coupon_noreuseyet="No puedes reutilizar este cup&oacute;n en este momento! Podr&aacute;s reutilizarlo el $date_placeholder";

// Banner delete error
$LANG_bannerdel_error="El banner no pudo ser eliminado porque ya fue quitado del intercambio! 
	Esto podria deberse a varios motivos. Vuelve a la pantalla de display de Banners y asegurate de que el banner aun existe!";

// e-mail change error
 $LANG_infoconfirm_invalid="El sistema fue incapaz de validar tu direcci&oacute;n de e-mail porque contiene caracteres especiales 
 	o es inv&aacute;lida. Por favor int&eacute;ntalo de nuevo o contacta al administrador del sistema para m&aacute;s asistencia.";

 // Password change errors
$LANG_err_nopassmatch="Las contrase&ntilde;as no coinciden!";
$LANG_err_passtooshort="La informaci&oacute;n enviada en inv&aacute;lida. (debe tener al menos 4 caracteres.";
?>