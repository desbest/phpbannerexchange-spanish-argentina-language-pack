<?
$file_rev="041305";
$file_lang="en";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

//Common messages
//menu:
$LANG_menu_nav="Navigaci&oacute;n";
$LANG_menu_home="Home";
$LANG_menu_logout="Logout";

$LANG_menu_stats="Estad&iacute;sticas";
$LANG_menu_emstats="Estad&iacute;sticas por E-mail";

$LANG_menu_site="Mi sitio";
$LANG_menu_commerce="Comprar cr&eacute;ditos";
$LANG_menu_banners="Mis Banners";
$LANG_menu_cat="Cambiar Categor&iacute;a";
$LANG_menu_htmlcode="Obtener c&oacute;digo HTML";

$LANG_menu_info="Mis Datos";
$LANG_menu_changeem="Modificar E-mail";
$LANG_menu_changepass="Modificar Contrase&ntilde;a";
$LANG_coupon_menuitem="Ingresa c&oacute;digo de Promo";

//Common stuff
$LANG_reset="Restaurar";

// Stats Page (client/stats.php)
$LANG_stats_title="Panel de Control para";
  //Stats Window stuff
$LANG_stats_startdate="Fecha de inicio";
$LANG_stats_siteexpos="Banners expuestos en tu sitio";
$LANG_stats_siteclicks="Clicks desde tu sitio";
$LANG_stats_percent="Porcentaje";
$LANG_stats_ratio="Ratio";
$LANG_stats_exposures="Exposiciones";
$LANG_stats_avgexp="Promedio de Exposiciones/D&iacute;a";
$LANG_stats_clicks="Clicks hacia tu sitio";
$LANG_commerce_credits="Cr&eacute;ditos";
  // Explanation of stats Window Stuff. These next 2 groups
  // are separate even though they say roughly the same thing
  // to keep them easier to manage.
  // 
  // This first one is for a normal x:1 ratio site..Full message
  // reads: "To date, you have displayed [x] banners on your site,
  // and generated [y] clicks from those exposures. You have earned
  // [z] credits for your own banner on other sites (you earn [n]
  // exposure(s) for each banner you display)."
$LANG_stats_exp_normal="<br>Hasta hoy, has expuesto ";
$LANG_stats_exp_normal1="banners en tu sitio, y generado";
$LANG_stats_exp_normal2="clicks a partir de esas exposiciones.  Has ganado ";
$LANG_stats_exp_normal3="cr&eacute;ditos para tu propio banner en otros sitios (obten&eacute;s"; 
$LANG_stats_exp_normal4="exposiciones por cada banner que expon&eacute;s).";
  // This one is for "odd" ratio sites like 5:4 ..Full message
  // reads: "To date, you have displayed [x] banners on your site,
  // and generated [y] clicks from those exposures. You have earned
  // [z] credits for your own banner on other sites (you earn [n]
  // exposure(s) for each [n] displays)."
$LANG_stats_exp_weird="<br>Hasta hoy, has expuesto";
$LANG_stats_exp_weird1="banners en tu sitio, y generado";
$LANG_stats_exp_weird2="clicks a partir de esas exposiciones.  Has ganado";
$LANG_stats_exp_weird3="cr&eacute;ditos para tu propio banner en otros sitios (obten&eacute;s"; 
$LANG_stats_exp_weird4="exposici&oacute;n por cada <b>$banexp</b> banners expuestos en tu sitio.)";
  // Approved or not approoved messages.
$LANG_stats_approved="Tu cuenta est&aacute; activa y participa en la rotaci&oacute;n del intercambio.";
$LANG_stats_unapproved="Tu cuenta aguarda aprobaci&oacute;n administrativa.  Una vez aprobada, ingresar&aacute; al sistema de rotaci&oacute;n, y se te asignaran los creditos ya ganados hasta entonces.";
$LANG_stats_bannercount="Banners en total:";
 // Referral messages. Reads: "You have earned [x] credits by
 // referring [y] account(s) to the exchange. Currently, there
 // are [z] accounts referred by you awaiting validation..(etc)".
$LANG_stats_referral1="Has ganado";
$LANG_stats_referral2="cr&eacute;ditos por referir";
$LANG_stats_referral3="cuenta(s) al intercambio. Actualmente hay";
$LANG_stats_referral4="cuentas referidas por vos aguardando validaci&oacute;n. Referral bounties are only paid out when the account is approved by the Exchange Administrator.";
  // Tips
$LANG_tip_startdate="La fecha en que fue creada tu cuenta.";
$LANG_tip_siteexposure="La cantidad de banners mostrados en tu sitio.";
$LANG_tip_clickfrom="La cantidad de clicks generados desde banners mostrados en tu sitio.";
$LANG_tip_percentout="El porcentaje de visitantes de tu sitio que hicieron click en un banner hacia otro sitio del intercambio.";
$LANG_tip_ratioout="La raz�n de visitantes de tu sitio que hicieron click en un banner hacia otro sitio del intercambio.";
$LANG_tip_exposures="La cantidad de veces que tu banner ha sido mostrado en otros sitios del intercambio.";
$LANG_tip_avgexp="El promedio de cantidad de exposiciones por d�a de tus banners en otros sitios del intercambio.";
$LANG_tip_clicks="La cantidad de veces que se ha hecho click sobre tus banners al ser expuestos en otros sitios del intercambio.";
$LANG_tip_percentin="El porcentaje de visitantes de los dem�s sitios del intercambio que hicieron click en tu banner.";
$LANG_tip_ratioin="La raz�n de visitantes de otros sitios del intercambio que hicieron click en tu banner.";
$LANG_tip_credits="La cantidad disponible de cr&eacute;ditos que tu banner ha acumulado por exponer banners en tu sitio.";

// Log Out page (/client/logout.php)
$LANG_logout_title="Logged Out";
$LANG_logout_message="Has cerrado tu cuenta exitosamente!<p><a href=\"../index.php\">Click aqui</a> para retornar a la pantalla de login.";

// Email Stats (/client/emailstats.php)
$LANG_emailstats_title="Estad&iacute;sticas por Email";
$LANG_emailstats_msg="Hemos enviado un an e-mail a $email con las estad&iacute;sticas de tu cuenta. Deber&iacute;as recibirlo enseguida.";

// Commerce/Buy Credits (/client/commerce.php)
$LANG_commerce_noitems="En este momento no hay creditos en venta";
$LANG_commerce_name="Nombre del producto";
$LANG_commerce_price="Precio";
  // "Buy now via [service]". In the future, phpBannerExchange
  // will support multiple payment services.
$LANG_commerce_buynow_button="Comprar ahora v&iacute;a";
$LANG_commerce_buynow="Comprar ahora";
$LANG_commerce_history="Tu historial de compras";
$LANG_commerce_date="Fecha";
$LANG_commerce_item="Item";
$LANG_commerce_purchaseprice="Valor de la compra";
$LANG_commerce_invoice="Factura";
$LANG_commerce_nohist="No se han encontrado compras!";
$LANG_commerce_couponhead="Cup&oacute;n";
$LANG_commerce_coupon_button="Aplicar Cup&oacute;n";

// Banners (/client/banners.php)
$LANG_targeturl="URL de destino";
$LANG_filename="Nombre de archivo";
$LANG_views="Vistas";
$LANG_clicks="Clicks";
$LANG_bannerurl="URL del Banner";
$LANG_menu_target="Modificar URL(s)";
$LANG_button_banner_del="Eliminar Banner";
$LANG_stats_hdr_add="Agregar un Banner";
$LANG_banner_instructions="Para editar la URL o el Destino de un banner, modifica la informaci�n en el campo apropiado, luego haz click sobre el bot&oacute;n <b>Modificar URL(s)</b>. Para eliminar un banner, haz click en el link <b>Eliminar Banner</b>. Para visitar el sitio especificado en la URL de Destino, haz click sobre el banner correspondiente.";

// this variable displays at the bottom of the "Banners" page.
// eg: "4 banner(s) found for your account"
$LANG_banner_found="banner(s) encontrados en tu cuenta";
$LANG_stats_nobanner="No hay banners en tu cuenta!";

// Delete Banner (/client/deletebanner.php and /client/deleteconfirm.php)
$LANG_delban_title="Eliminar Banner";
$LANG_delban_warn="Est&aacute;a seguro de borrar este banner? Este procedimiento no puede dehacerse.<br>";
$LANG_delban_button="SI, eliminar este baner";
$LANG_delbanconf_verbage="El banner ha sido eliminado!";
$LANG_delbanconf_success="El banner ha sido eliminado de tu cuenta exitosamente";

// Category page (/client/category.php and /client/categoryconfirm.php)
$LANG_cat_reval_warn="La modificaci&oacute;n de tu categor&iacute;a requerir&aacute; re-aprobaci&oacute;n del administrador. (igualmente ganar&aacute;s cr&eacute;ditos mientras tu cuenta espera aprobaci&oacute;n).";
$LANG_cat_change_button="Cambiar Categor&iacute;a";
$LANG_cat_nocats="No hay categoria definidas, por lo tanto no es posible cambiar de categor&iacute;a en este momento.";
$LANG_catconf_message="Tu categor&iacute;a ha sido modificada!";

// Get HTML (/client/gethtml.php)
$LANG_gethtml_title="Obtener HTML";
$LANG_gethtml_message="Si queres usar la caracter&iacute;stica de categor&iacute;as para mostrar banners mas adecuados segun categor&iacute;a en tu sitio, reemplaza \"&cat=0\" con el n&uacute;mero de la categor&iacute;a apropiado segun la tabla de abajo (ejemplo: \"&cat=2\", \"&cat=3\", y asi sucesivamente). Verifica que uses el mismo numero para ambas ocasiones en el codigo. Esto asegurar&aacute; que recibas el cr&eacute;dito debido por tus exposiciones. Si dejas este valor como est&aacute;, permitir&aacute; que se expongan banners de todos las cuentas sin importar la categor&iacute;a.";
$LANG_gethtml_catname="Nombre de la Categor&iacute;a";
$LANG_gethtml_catid="ID de la Categor&iacute;a";

// Change email address (/client/editinfo.php)
$LANG_email_title="Editar tu Cuenta";
$LANG_email_address="Direcci&oacute;n de Email";
$LANG_email_button="Modificar Email";

// Change email confirmation (/client/editconfirm.php)
 $LANG_infoconfirm_title="Editar tu Cuenta";
 $LANG_infoconfirm_success="Tu direcci&oacute;n de Email ha sido cambiada por ";

// Change PW form (/client/editpass.php)
$LANG_pass_title="Cambiar Contrase&ntilde;a";
$LANG_pass1_label="Nueva Contrase&ntilde;a";
$LANG_pass2_label="Otra vez";
$LANG_pass_button="Cambiar Contrase&ntilde;a";
$LANG_pass_confirm="Tu Contrase&ntilde;a ha sido modificada! Necesitaras <a href=\"logout.php\">desloguearte</a> y volver a loguearte con tu nueva Contrase&ntilde;a.";

// Buy Credits page (/client/promo.php)
$LANG_coupon_menuitem="Ingresar c&oacute;digo de promoci&oacute;n";
$LANG_coupon_instructions="Ingresa abajo el c&oacute;digo de promoci&oacute;n recibido de la administraci&oacute;n.";
$LANG_submit="Enviar";
$LANG_coupon_success="<b>El cup&oacute;n ha sido acreditado!</b>";
$LANG_coupon_success2="$credits cr&eacute;ditos han sido asignados a tu cuenta y ya estan disponibles.";

// Click Log (/client/clicklog.php)
$LANG_clicklog="Registro de Clicks";
$LANG_clicklog_from="Desde tu sitio";
$LANG_clicklog_to="Hacia tu sitio";
$LANG_clicklog_ip="Direcci&oacute;n IP";
$LANG_clicklog_date="Fecha/Hora";
$LANG_noclicks="No hay clicks para mostrar.";


//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!
?>