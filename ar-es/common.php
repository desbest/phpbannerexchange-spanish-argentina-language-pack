<?
$file_rev="041305";
$file_lang="en";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// Menu items..
$LANG_menu_options="Opciones";
$LANG_backtologin="Login";
$LANG_lostpw="Perdiste tu contrase&ntilde;a?";
$LANG_faq="FAQ";
$LANG_signup="Registrarme";
$LANG_rules="Reglas del Intercambio";
$LANG_tocou="Terminos del Servicio";

$LANG_menu_extras="Extras";
$LANG_topbanns="Top Banners";
$LANG_overallstats="Estad&iacute;sticas generales";

// login page (/index.php)
$LANG_indtitle="Login al Panel de Control";
$LANG_headertitle="Panel de Control del Intercambio de Banners";
$LANG_login_instructions="Ingresa tu nombre de usuario y contrase&ntilde;a para acceder a tu cuenta. 
	Si deseas unirte a este Intercambio de Banners, <a href=\"cou.php\">haz click aqu&iacute;</a> para abrir una cuenta!";
$LANG_login="Nombre de Usuario:";
$LANG_pw="Contrase&ntilde;a:";
$LANG_login_button="Login";

//Recover Password (/recoverpw.php)
$LANG_lostpw_title="Recuperar contrase&ntilde;a";
$LANG_lostpw_instructions="Para reestablecer tu contrase&ntilde;a, ingresa la direcci&oacute;n de e-mail utilizada para registrarte. 
	Se crear&aacute; una nueva contrase&ntilde;a y ser&aacute; enviada a esa direcci&oacute;n.";
$LANG_lostpw_instructions2=" Por razones de seguridad no es posible enviar la contrase&ntilde;a reestablecida a una direccion de e-mail diferente.";
$LANG_lostpw_recover="Tu contrase&ntilde;a ha sido reestablecida, te la hemos enviado por e-mail.";
$LANG_lostpw_email="Direcci&oacute;n de E-mail";
$LANG_lostpw_success="Tu contrase&ntilde;a ha sido reestablecida! te la hemos enviado por e-mail. 
	Una vez que hayas ingresado, podras cambiarla desde el Panel de Control.";

// Signup Form (/signup.php)
$LANG_signupwords="Registrarme en $exchangename";
$LANG_realname="Nombre Real";
$LANG_pw_again="Repetir contrase&ntilde;a";
$LANG_cat="Categor&iacute;a";
$LANG_catstuff="Por favor selecciona una Categor&iacute;a";
$LANG_email="Direcci&oacute;n de Email";
$LANG_siteurl="URL del Sitio";
$LANG_bannerurl="URL del Banner";
$LANG_signsub="Haz click una vez para enviar";
$LANG_signres="Restablecer";
$LANG_newsletter="Enviar Newsletter";
$LANG_coupon="C&oacute;digo del Cup&oacute;n";
$LANG_rejected="No fue posible crear tu cuenta por los siguientes motivos:";

// success messages..
$LANG_signup_thanks="Gracias por registrarte!";
$LANG_signupinfo="Tu cuenta fue creada exitosamente! 
	Ser&aacute;s agregado a la rotaci&oacute;n de banners dentro de los pr&oacute;ximos dos d&iacute;as. 
	Tambi&eacute;n hemos enviado un email a $email con tu nombre de usuario y contrase&ntilde;a. 
	Debes guardar esta informaci&oacute;n en un lugar seguro futura referencia.  
	Puedes <a href=\"index.php\">loguearte</a> en cualquier momento.  Este panel te permitir&aacute; agregar banners, 
	consultar estad&iacute;sticas, modificar la URL, cambiar contrase&ntilde;a, etc.<p>";
$LANG_coupon_added="Tu cuenta ha sido acreditada con <b>$newcredits</b> cr&eacute;ditos por usar este cup&oacute;n.";
$LANG_signup_uploadmsg="Nota: Necesitar&aacute;s ingresar y cargar un banner a fin de que tu cuenta sea aprobada!";

// Conditions of Use page (/conditions.php)
$LANG_coutitle="T&eacute;rminos y Condiciones";
$LANG_header="Acuerdo de los Miembros";
$LANG_agree="Estoy de Acuerdo";
$LANG_disagree="No estoy de acuerdo";

// Top banners/accounts page (/top.php)
//top 10 banners:
$LANG_top10_title="$topnum cuentas Top";
$LANG_top10_exposure="Exposiciones";
$LANG_top10_banners="Banner";
$LANG_top10_nobanners="Aun no hay Banners aprobados en el Intercambio, por lo que no hay datos para mostrar aqui! 
	Nota: No se muestran los banners del sistema en esta p&aacute;gina, solo los de cuentas de usuarios!";

//Overall stats (/overall.php)
$LANG_overall_totusers="Usuarios";
$LANG_overall_exposures="Exposiciones";
$LANG_overall_banners="Banners";
$LANG_overall_totclicks="Clicks hacia sitios";
$LANG_overall_totsiteclicks="Clicks desde sitios";
$LANG_overall_ratio="Click-thru ratio";


// common stuff...
$LANG_yes="Si";
$LANG_no="No";
$LANG_top="Top";
$LANG_topics="Temas";

//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!
?>